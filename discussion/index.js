// s23 discussion

// CRUD Operations

// create
// db.collectionName.insertOne( {} )
// db.collectionName.insert( {} )

db.users.insert({
    
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
        },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
    
    })

// db.collectionName.insertMany( {} )
db.users.insertMany([{
    
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
        },
    courses: ["Python", "React", "PHP"],
    department: "none"
    
    },
    {
    
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "87654321",
        email: "neilarmstrong@gmail.com"
        },
    courses: ["React", "RLaravel", "Sass"],
    department: "none"
  
  }]
    )

// read - finding documents
// db.collectionName.find( {field: value} )

db.users.find({ firstName: "Stephen" })

// if documents match the criteria, the document will return the match found

db.users.find({})

// leaving the search criteria empty, it will return all of the documents

// update - updating documents
// db.collectionName.updateOne({criteria}, {$set: {field: value}})

db.users.updateOne({firstName: "Test"}, {$set: {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
        email: "billgates@email.com"
        },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations",
    status: "active"
    }})

// db.collectionName.updateMany({criteria}, {$set: {field: value}})
db.users.updateMany({department: "none"}, {$set: {
    department: "HR"
    }})


// db.collectionName.replaceOne({criteria}, {fields that need to be changed})
// it can be used if replacing the whole document is necessary
db.users.replaceOne({firstName: "Bill"},{
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact:{
        phone: "12345678",
        email: "bill@gmail.com"
        },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations"
    })

 // delete
 // db.collectionName.deleteOne({criteria})
 db.users.deleteOne({firstName: "test"})

// db.collectionName.deleteMany({criteria})
 db.users.deleteMany({})
 // if the field is blank the whole documents will be deleted so be careful

 //ADVANCE QUERIES
/*
- Retrieving data with complex data structures is also a good skill for any developer to have.
- Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
- Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/


// Query an embedded document
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
		}
})

// Query on nested field
db.users.find(
	{"contact.email": "janedoe@gmail.com"}
)

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } )

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } )

//Querrying an Embedded Array of objects
//insert data for demo:
db.users.insert({
    nameArray: [{
        nameA: "Juan"
    },
    {
        nameB: "Tamad"
    }

]
    }
)


//then:


db.users.find({
    
    nameArray: {nameA: "Juan"}
    
})

